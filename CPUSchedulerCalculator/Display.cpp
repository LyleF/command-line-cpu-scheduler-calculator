#pragma once
#include "stdafx.h"
#include "conio.h"

#pragma region Utilities

void addHorizontalLine(int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << "-";
	}
}

void checkProcessQueue(std::queue<SchedulerData> *dataQueue)
{
	out("");
	addHorizontalLine(5);
	std::cout << "Gantt Chart:";
	addHorizontalLine(5);
	out("");

	std::queue<SchedulerData>  cachedQueue = *dataQueue;
	while (dataQueue->empty() == false)
	{
		SchedulerData front = dataQueue->front();
		std::cout << "|   " << front.processChar << "   ";
		dataQueue->pop();
	}
	std::cout << "| \n";

	int index = 0;
	int sizeOfQueue = cachedQueue.size();
	while (cachedQueue.empty() == false)
	{
		SchedulerData front = cachedQueue.front();
		if (index == 0 || index == sizeOfQueue)
			std::cout << front.responseTime << "       " << front.timeFinished;
		else
			std::cout << "       " << front.timeFinished;

		index++;
		cachedQueue.pop();
	}

	std::cout << std::endl << std::endl << std::endl;
}

void checkProcessTable(std::list<SchedulerData> *dataList) {

		addHorizontalLine(5);
		std::cout <<" Process Table ";
		addHorizontalLine(5);
		std::cout << /*"\n| Process | Waiting Time |  Turn Around Time  |":*/ "\n| Process | Arrival Time |  Burst Time  | Priority Level |";
		for (std::list<SchedulerData>::const_iterator it = dataList->begin(); it != dataList->end(); ++it)
		{

			std::cout << "\n|    " << it->processChar << "    |";
				if (it->arrivalTime > 9)
				{
					std::cout << "     " << it->arrivalTime << "       |";
				}
				else {
					std::cout << "     " << it->arrivalTime << "        |";
				}

				if (it->burstTime > 9) {
					std::cout << "     " << it->burstTime << "       |";
				}
				else {
					std::cout << "     " << it->burstTime << "        |";
				}

				if (it->priorityLevel > 9)
				{
					std::cout << "      " << it->priorityLevel << "        |";
				}
				else {
					std::cout << "      " << it->priorityLevel << "         |";
				}
		}
	
	std::cout << std::endl << std::endl << std::endl;
}

void checkFinalProcessTable(std::list<SchedulerData> *dataList) 
{

	int totalWaitTime = 0;
	int totalTurnAroundTime = 0;
	int iterations = 0;
	//std::sort(dataArray, dataArray + dataList.size(), early)
	dataList->sort([](const SchedulerData &x, const SchedulerData &y) {return x.processChar < y.processChar;});
	addHorizontalLine(5);
	std::cout << " Process Table ";
	addHorizontalLine(5);
	std::cout << "\n| Process | Waiting Time |  Turn Around Time  |";
	for (std::list<SchedulerData>::const_iterator it = dataList->begin(); it != dataList->end(); ++it)
	{
		std::cout << "\n|    " << it->processChar << "    |";
		if (it->waitingTime > 9)
		{
			std::cout << "     " << abs(it->waitingTime) << "       |";
		}
		else {
			std::cout << "     " << abs(it->waitingTime) << "        |";
		}

		if (it->turnAroundTime > 9)
		{
			std::cout << "       " << abs(it->turnAroundTime) << "           |";
		}
		else {
			std::cout << "      " << abs(it->turnAroundTime) << "             |";
		}

		totalTurnAroundTime += it->turnAroundTime;
		totalWaitTime += it->waitingTime;
		++iterations;
	}

	std::cout << "\nAverage Waiting Time: " << (double)totalWaitTime / iterations << std::endl;
	std::cout << "Average Turn Around Time: " << (double)totalTurnAroundTime / iterations << std::endl;

	std::cout << std::endl << std::endl << std::endl;
}
#pragma endregion
#pragma region Display

Display::Display(std::list<SchedulerData> *dataList, std::list<SchedulerData> *currentAvailable, std::queue<SchedulerData> *queuedData, bool forceDisp)
{
	this->forceDisp = forceDisp;
	this->currentAvailableData = currentAvailable;
	this->currentDataList = dataList;
	this->currentQueuedData = queuedData;
}

void Display::onEnter(StateMachine *machine)
{
	if (this->forceDisp == false) 
	{
		checkProcessTable(this->currentDataList);
		checkProcessQueue(this->currentQueuedData);
		checkFinalProcessTable(this->currentAvailableData);
	}
	char n;
	std::cout << "\n Compute again? Y/N";
	std::cin >> n;
	
	if (n == 'Y' || n == 'y') 
	{
		machine->setCurrent(new Input());
	}
	else
	{
		exit(0);
	}
}

void Display::displayProcessTable(bool isFinal)
{
	checkProcessTable(this->currentDataList);
	getchar();
}

void Display::onExit(StateMachine *machine)
{

}

#pragma endregion