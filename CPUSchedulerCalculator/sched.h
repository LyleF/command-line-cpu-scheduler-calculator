class sched
{
public:
	int n, bt[10], at[10], tat[10], wt[10], rt[10], finish[10], twt, ttat, total;
	void readData(SchedulerData array[], int size );
	void Init();
	void dispTime(SchedulerData dataArray[]);
	int getNextProcess(int);
	void computeRR(SchedulerData dataArray[]);
	void computeSRT(SchedulerData dataArray[]);
};
