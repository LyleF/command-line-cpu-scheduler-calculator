#pragma once
#include "InputFSM.h"
class Left : public State
{
	public:
		void onEnter(StateMachine *fsm) override 
		{
			out("Going Left...");
		}

		void onExit(StateMachine *fsm) override 
		{
			out("Exit Left...");
		}
};

class Right : public State {
public:
	void onEnter(StateMachine *fsm) override
	{
		out("Going Right...");
	}

	void onExit(StateMachine *fsm) override
	{
		out("Exit Right...");
	}
};