#pragma once
#include "stdafx.h"
#include "list"
#include "queue"

class SchedulerData 
{
	public:
		bool isInitialized = false;
		char processChar;
		int arrivalTime;
		int burstTime;
		int priorityLevel;

		int remianingTime;
		int timeFinished;
		int responseTime;
		int waitingTime;
		int turnAroundTime;
};

#pragma region Utilities
void addHorizontalLine(int size);
void checkProcessQueue(std::queue<SchedulerData> *dataQueue);
void checkProcessTable(std::list<SchedulerData> *dataList);
void checkFinalProcessTable(std::list<SchedulerData> *dataList);
#pragma endregion

class SchedulerAlgorithm : public StateMachine
{
	//class std::list<SchedulerData> *currentDataList;
	public:
		SchedulerAlgorithm(std::list<SchedulerData> *dataList, StateMachine* mainMachine)
		{
			currentDataList = dataList;
			programMachine = mainMachine;
			currentProcessQueue = new std::queue<SchedulerData>();
			currentAvailableDataList = new std::list<SchedulerData>();
		}
		StateMachine * programMachine;
		std::list<SchedulerData> *currentDataList;

		std::list<SchedulerData> *currentAvailableDataList;
		std::queue<SchedulerData> *currentProcessQueue;
		int currentTime;
};

#pragma region CPU Scheduler classes
class FirstComeFirstServe : public SchedulerAlgorithm 
{
	public:
		FirstComeFirstServe(std::list<SchedulerData> *dataList, StateMachine* mainMachine) : SchedulerAlgorithm(dataList, mainMachine){}
};

class ShortestJobFirst_NP : public SchedulerAlgorithm 
{
	public:
		ShortestJobFirst_NP(std::list<SchedulerData> *dataList, StateMachine* mainMachine) : SchedulerAlgorithm(dataList, mainMachine) {};

};
class ShortestRemainingTime_P : public SchedulerAlgorithm
{
	public:
		ShortestRemainingTime_P(std::list<SchedulerData> *dataList, StateMachine* mainMachine) : SchedulerAlgorithm(dataList, mainMachine) {};

};
class Priority_NP : public SchedulerAlgorithm 
{
	int pr[10], id[10], exe[10], ar[10];
	int n;
	void sort(int*, int*, int*, int *);
	public:
	void getdata(SchedulerData dataArray[], int size);
	//void display(SchedulerData dataArray[]);
	void cal_wt_tt();
	Priority_NP(std::list<SchedulerData> *dataList, StateMachine* mainMachine) : SchedulerAlgorithm(dataList, mainMachine) {};

};

class Priority_P : public SchedulerAlgorithm 
{
	public:
		Priority_P(std::list<SchedulerData> *dataList, StateMachine* mainMachine) : SchedulerAlgorithm(dataList, mainMachine) {};
		void init(SchedulerData dataArray[], int size);

		void drawGanttChart();
		void calculateProcessSequence();
		int findAptProcessNumber(int);
		int numberOfProcesses, totalCPUBurstTime, *arrivalTime, *CPUBurstTime, *CPUBurstTimeCopy, *processNumber, minimumArrivalTime, *processSequenceForEachSecond, *waitingTime, *priority;
		float  averageTurnAroundTime;
		float averageWaitingTime = 0;

		/*arrays used to draw Gantt Chart*/
		int *processNumberGantt, *CPUBurstTimeGantt, ganttSize;
};

class RoundRobin : public SchedulerAlgorithm 
{
	public:
		void init(SchedulerData dataArray[], int size);

		void search01(int pnt, int tm);
		void search02(int pnt, int tm);
		void addqueue(int pnt);
		void disp();
		int at[50], bt[50], ct[50] = { 0 }, qt, rqi[50] = { 0 }, c = 0, st, flg = 0, tm = 0, noe = 0, pnt = 0, btm[50] = { 0 }, tt, wt,  size;
		float att, awt;

		RoundRobin(std::list<SchedulerData> *dataList, StateMachine* mainMachine) : SchedulerAlgorithm(dataList, mainMachine) {};

};
#pragma endregion

#pragma region Scheduler States
class CheckArrivalState : public State 
{
	public: 
		void onEnter(StateMachine *machine) override;
		void onExit(StateMachine *machine) override;
	private:
		void checkAvailable(std::list<SchedulerData> *currentDataList, std::list<SchedulerData> *currentAvailableDataList);
};

class CheckBurstState : public State
{
public:
	void onEnter(StateMachine *machine) override;
	void onExit(StateMachine *machine) override;
};

class CheckPriorityState : public State
{
public:
	void onEnter(StateMachine *machine) override;
	void onExit(StateMachine *machine) override;
};

class EnqueueState : public State 
{
	public:
		//EnqueueState();
		void onEnter(StateMachine *machine) override;
		void onExit(StateMachine *machine) override;
};

class PreEmptState : public State {
	public:
		void onEnter(StateMachine *machine) override;
		void onExit(StateMachine *machine) override;
};

class FinalizeState : public State 
{
public:
	void onEnter(StateMachine *machine) override;
	void onExit(StateMachine *machine) override;
};
#pragma endregion
