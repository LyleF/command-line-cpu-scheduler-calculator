#pragma once

#include <queue>
#include "iostream"

class QueueTest 
{
	public:
		void inputTest();
		void outputContent();
		std::queue<int> testQueue;
};