#pragma once
#include "stdafx.h"
#include "conio.h"


void Input::onEnter(StateMachine *machine)
{
	this->displayMenu(machine);
}

void Input::displayMenu(StateMachine *machine)
{
	system("cls");
	addHorizontalLine(5);
	std::cout << "CPU Scheduling Algorithm Calculator";
	addHorizontalLine(5);

	std::cout << "\n (1) Start \n";
	std::cout << "\n (2) Exit  \n";

	std::cout << "\nInput choice: ";
	waitForInput(machine, true);
}

void Input::getInputData(StateMachine *machine)
{
	system("cls");
	addHorizontalLine(5);
	std::cout << "Input data";
	addHorizontalLine(5);

	int numberOfProcesses = 0;

	char msg[] = "\n How many processes do you want? ";
	numberOfProcesses = validateNumberInput(msg);

	std::list<SchedulerData> *dataList = new std::list<SchedulerData>;
	setSchedulerData(dataList, numberOfProcesses);
	displaySchedulerChoices(machine);
	//machine->setCurrent(new Display(dataList));
}

int Input::validateNumberInput(char msg[])
{
	std::string str_n = "";
	int n = -1;

	std::cout << msg;
	while (std::getline(std::cin, str_n) && str_n.end() != std::find_if_not(str_n.begin(), str_n.end(), &isdigit) || str_n.empty())
	{
		std::cout << msg;
	}

	n = std::stoi(str_n);
	return n;
}

void Input::setSchedulerData(std::list<SchedulerData> *dataList, int amount)
{
	char processChar = 65;
	int arrivalTime;
	int burstTime;
	int priorityLevel;

	for (int i = 0; i < amount; i++)
	{
		system("cls");
		checkProcessTable(dataList);

		addHorizontalLine(5);
		std::cout << "Input Data Creation";
		addHorizontalLine(5);
		std::cout << "\nNumber of Processes: " << amount << std::endl;

		std::cout << "Process [" << processChar << "] " << std::endl;

		char msg[] = "Arrival time: ";
		arrivalTime = validateNumberInput(msg);
		char msg1[] = "Burst Time: ";
		burstTime = validateNumberInput(msg1);
		char msg2[] = "Priority Level: ";
		priorityLevel = validateNumberInput(msg2);

		SchedulerData dataPackage;
		dataPackage.processChar = processChar++;
		dataPackage.arrivalTime = arrivalTime;
		dataPackage.burstTime = burstTime;
		dataPackage.priorityLevel = priorityLevel;
		dataPackage.isInitialized = true;

		dataList->emplace_back(dataPackage);
	}

	this->currentDataList = dataList;
}

void Input::displaySchedulerChoices(StateMachine *machine)
{
	system("cls");
	checkProcessTable(this->currentDataList);

	addHorizontalLine(5);
	std::cout << "CPU Scheduling Algorithms";
	addHorizontalLine(5);

	std::cout << "\n (1) First Come First Serve \n";
	std::cout << "\n (2) Shortest Job First (Non Pre-Emptive) \n";
	std::cout << "\n (3) Shortest Remaining Time (Pre-Emptive) \n";
	std::cout << "\n (4) Priority (Non Pre-Emptive) \n";
	std::cout << "\n (5) Priority (Pre-Emptive) \n";
	std::cout << "\n (6) Round Robin \n";

	std::cout << "\nInput choice: ";
	waitForInput(machine, false);
}

void Input::waitForInput(StateMachine *machine, bool fromMenu)
{
	while (true)
	{
		while (_kbhit())
		{
			choice = _getch();
			std::cout << choice;

			if (fromMenu)
			{
				switch (choice)
				{
				case '1':
					getInputData(machine);
					goto here;
				case '2':
					exit(0);
					break;
				default:
					displayMenu(machine);
					goto here;
				}
			}
			else {
				switch (choice)
				{
				case '1': {
					SchedulerAlgorithm * base_fcfs = new FirstComeFirstServe(this->currentDataList, machine);
					FirstComeFirstServe * fcfs = dynamic_cast<FirstComeFirstServe*>(base_fcfs);
					fcfs->setCurrent(new CheckArrivalState());
					goto here;
				}
				case '2': {
					SchedulerAlgorithm * base_sjf = new ShortestJobFirst_NP(this->currentDataList, machine);
					ShortestJobFirst_NP * sjf_np = dynamic_cast<ShortestJobFirst_NP*>(base_sjf);
					sjf_np->setCurrent(new CheckArrivalState());
					goto here;
				}
				case '3': {
					SchedulerAlgorithm * base_srt = new ShortestRemainingTime_P(this->currentDataList, machine);
					ShortestRemainingTime_P * srt_p = dynamic_cast<ShortestRemainingTime_P*>(base_srt);
					srt_p->setCurrent(new CheckArrivalState());
					goto here;
				}
				case '4': {
					SchedulerAlgorithm * base_pnp = new Priority_NP(this->currentDataList, machine);
					Priority_NP * p_np = dynamic_cast<Priority_NP*>(base_pnp);
					p_np->setCurrent(new CheckArrivalState());
					goto here;
				}
				case '5': {
					SchedulerAlgorithm * base_p = new Priority_P(this->currentDataList, machine);
					Priority_P * p_p = dynamic_cast<Priority_P*>(base_p);
					p_p->setCurrent(new CheckArrivalState());
					goto here;
				}
				case '6': {
					SchedulerAlgorithm * base = new RoundRobin(this->currentDataList, machine);
					RoundRobin * rr = dynamic_cast<RoundRobin*>(base);
					rr->setCurrent(new CheckArrivalState());
					goto here;
				}
				}
			}

			if (choice == 27)
			{
				goto here;
			}
		}
	}

here:

	std::cout << "\n";
}

void Input::onExit(StateMachine *machine)
{
	system("cls");
}