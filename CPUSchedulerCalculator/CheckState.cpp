#pragma once
#include "stdafx.h"
int tempFinished;
bool compare(SchedulerData x, SchedulerData y) 
{
	return (x.burstTime < y.burstTime && x.arrivalTime <= tempFinished);
}

bool early(SchedulerData x, SchedulerData y) 
{
	return x.arrivalTime < y.arrivalTime;
}

int getNextProcess(int time,int size, SchedulerData dataArray[])
{
	int i, low = 0;
	for (i = 0;i<size;i++)
		if (dataArray[i].timeFinished == 0) { low = i; break; }

	for (i = 0;i<size;i++)
		if (dataArray[i].timeFinished != 1)
			if (dataArray[i].remianingTime<dataArray[low].remianingTime && dataArray[i].arrivalTime <= time)
				low = i;
	return low;
}

void CheckArrivalState::onEnter(StateMachine *machine) 
{
	SchedulerAlgorithm *scheduler = dynamic_cast<SchedulerAlgorithm*>(machine);
	SchedulerData dataArray[10];
	//this->checkAvailable(scheduler->currentDataList, scheduler->currentAvailableDataList);
	
	int listSize = 0;
#pragma region  FCFS
	if (dynamic_cast<FirstComeFirstServe*>(scheduler)) 
	{
		std::list<SchedulerData> dataList = *scheduler->currentDataList;
		dataList.sort([](const SchedulerData &x, const SchedulerData &y) {return x.arrivalTime < y.arrivalTime;});
		*scheduler->currentAvailableDataList = dataList;
		
		SchedulerData prevData;
		for (std::list<SchedulerData>::iterator it = scheduler->currentAvailableDataList->begin(); it != scheduler->currentAvailableDataList->end(); ++it)
		{
			SchedulerData &data = *it;

			if (dynamic_cast<FirstComeFirstServe*>(machine) || dynamic_cast<ShortestJobFirst_NP*>(machine))
			{
				if (it == scheduler->currentAvailableDataList->begin())
				{
					data.responseTime = data.arrivalTime;
				}
				else
				{
					data.responseTime = prevData.timeFinished;
				}

					data.timeFinished = data.burstTime + data.responseTime;
					prevData = data;

					data.waitingTime = data.responseTime - data.arrivalTime;
					data.turnAroundTime = data.timeFinished - data.arrivalTime;
			}
		}
		scheduler->setCurrent(new EnqueueState);
	}

#pragma endregion

	std::list<SchedulerData> dataList = *scheduler->currentDataList;
	//dataList.sort([](const SchedulerData &x, const SchedulerData &y) {return x.arrivalTime < y.arrivalTime;});

	int k = 0;
	for (SchedulerData const &d : dataList)
	{
		dataArray[k++] = d;
	}

	listSize = dataList.size();

#pragma region SJF_NP


	if (dynamic_cast<ShortestJobFirst_NP*>(scheduler))
	{
		//out("before sort");
		//for (int i = 0; i < 5; i++)
		//{
		//	std::cout << dataArray[i].processChar;
		//}

		std::sort(dataArray, dataArray + dataList.size(), early);

		dataArray[0].responseTime = dataArray[0].arrivalTime;
		dataArray[0].timeFinished = dataArray[0].burstTime + dataArray[0].arrivalTime;
		dataArray[0].turnAroundTime = dataArray[0].timeFinished - dataArray[0].arrivalTime;
		dataArray[0].waitingTime = dataArray[0].turnAroundTime - dataArray[0].burstTime;

		for (int i = 1;i<dataList.size();i++)
		{
			tempFinished = dataArray[i - 1].timeFinished;
			std::sort(dataArray + i, dataArray + dataList.size(), compare);
			
			if (dataArray[i - 1].timeFinished <dataArray[i].arrivalTime)
			{
				dataArray[i].timeFinished = dataArray[i - 1].timeFinished + dataArray[i].burstTime + (dataArray[i].arrivalTime- dataArray[i - 1].timeFinished);
			}
			else
			{

				dataArray[i].timeFinished = dataArray[i - 1].timeFinished + dataArray[i].burstTime;

			}
			dataArray[i].turnAroundTime = dataArray[i].timeFinished - dataArray[i].arrivalTime;
			dataArray[i].waitingTime = dataArray[i].turnAroundTime - dataArray[i].burstTime;
		}

		//out("");
		//out("after sort");
		//for (int i = 0; i < 5; i++)
		//{
		//	std::cout << dataArray[i].processChar;
		//}

		//getchar();
		//getchar();

		dataList.clear();

		for (int i = 0; i < listSize; i++) 
		{
			if (dataArray[i].isInitialized == false)
				continue;

			dataList.emplace_back(dataArray[i]);
		}

		*scheduler->currentAvailableDataList = dataList;
		scheduler->setCurrent(new EnqueueState);
	}
#pragma endregion

#pragma region SRT_P

	if (dynamic_cast<ShortestRemainingTime_P*>(scheduler))
	{
		sched *srt = new sched();

		std::sort(dataArray, dataArray + dataList.size(), early);
		srt->readData(dataArray, listSize);
		srt->computeSRT(dataArray);
		dataList.clear();

		for (int i = 0; i < listSize; i++)
		{
			if (dataArray[i].isInitialized == false)
				continue;

			dataList.emplace_back(dataArray[i]);
		}
		getchar();getchar();
		*scheduler->currentAvailableDataList = dataList;

		delete srt;
		scheduler->programMachine->setCurrent(new Display(scheduler->currentDataList, scheduler->currentAvailableDataList, scheduler->currentProcessQueue, true));
	}

#pragma endregion

#pragma region RoundRobin (FIX THIS ITS STILL BUGGY)
	if (dynamic_cast<RoundRobin*>(scheduler)) 
	{
		RoundRobin *rr = dynamic_cast<RoundRobin*>(scheduler);

		rr->init(dataArray, listSize);
		rr->disp();

		dataList.clear();

		for (int i = 0; i < listSize; i++)
		{
			if (dataArray[i].isInitialized == false)
				continue;

			dataList.emplace_back(dataArray[i]);
		}

		*scheduler->currentAvailableDataList = dataList;
		getchar();getchar();
		//delete rr;
		scheduler->programMachine->setCurrent(new Display(scheduler->currentDataList, scheduler->currentAvailableDataList, scheduler->currentProcessQueue, true));
	}
#pragma endregion

#pragma region Priority_NP
	if (dynamic_cast<Priority_NP*>(scheduler)) 
	{
		Priority_NP *pnp = dynamic_cast<Priority_NP*>(scheduler);
		pnp->getdata(dataArray, listSize);
		pnp->cal_wt_tt();
		getchar();getchar();

		scheduler->programMachine->setCurrent(new Display(scheduler->currentDataList, scheduler->currentAvailableDataList, scheduler->currentProcessQueue, true));
		delete pnp;
	}
#pragma endregion

#pragma region Priority_P
	if (dynamic_cast<Priority_P*>(scheduler))
	{
		Priority_P *p = dynamic_cast<Priority_P*>(scheduler);
		p->init(dataArray, listSize);
		scheduler->programMachine->setCurrent(new Display(scheduler->currentDataList, scheduler->currentAvailableDataList, scheduler->currentProcessQueue, true));
		delete p;
	}
#pragma endregion

}



void CheckArrivalState::onExit(StateMachine *machine) 
{
	
}