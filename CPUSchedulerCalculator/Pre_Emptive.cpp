#include "stdafx.h"
#include "iostream"
using namespace std;

void sched::readData(SchedulerData array[], int size)
{
	//cout << "Enter no. of processes\n";
	//cin >> n;
	//cout << "Enter the burst times in order :\n";
	n = size;
	for (int i = 0;i < n;i++)
		bt[i] = array[i].burstTime;
	//cout << "Enter the arrival times in order:\n";
	for (int i = 0;i < n;i++)
		at[i] = array[i].arrivalTime;
}



void sched::Init() 
{
	total = 0;
	twt = 0;
	ttat = 0;
	for (int i = 0; i<n; i++) {
		rt[i] = bt[i];
		finish[i] = 0;
		wt[i] = 0;
		tat[i] = 0;
		total += bt[i];
	}
}

void sched::computeSRT(SchedulerData dataArray[]) 
{
	//readData();
	Init();
	int time, next = 0, old, i;
	cout << "\nGantt Chart\n ";
	for (time = 0;time<total;time++)
	{
		old = next;
		next = getNextProcess(time);
		if (old != next || time == 0) 
		{
			cout << "(" << time << ")|==" << dataArray[next].processChar << "==|";
		}
		rt[next] = rt[next] - 1;
		if (rt[next] == 0) finish[next] = 1;
		for (i = 0;i<n;i++)
			if (i != next && finish[i] == 0 && at[i] <= time)
				wt[i]++;
	}

	cout << "(" << total << ")" << endl;

	for (i = 0;i<n;i++)
		if (!finish[i]) { cout << "Scheduling failed, cannot continue\n"; return; }

	dispTime(dataArray);

}
//n: Number of Processes
//	bt[] : Burst time for each process
//	at[] : Arrival time for each process
//	tat[] : Turnaround time for each process
//	wt[] : Waiting time for each process
//	rt[] : Remaining time for each process
//	finish[] : Process complete status
//	twt : Total wait time
//	ttat : Total Turnaround time
//	total : Total burst time
void sched::dispTime(SchedulerData dataArray[])
{
	std::cout << "\n| Process | Waiting Time |  Turn Around Time  |";
	for (int i = 0;i<n;i++)
	{

		twt += wt[i];
		tat[i] = wt[i] + bt[i];
		ttat += tat[i];
		//cout << "Waiting time for P" << (i + 1) << " = " << wt[i] << ", Turnaround time = " << tat[i] << endl;
		cout << "\n|    " << dataArray[i].processChar << "    |";

		if (wt[i] > 9)
		{
			std::cout << "     " << wt[i] << "       |";
		}
		else {
			std::cout << "     " << abs(wt[i]) << "        |";
		}

		if (tat[i] > 9)
		{
			std::cout << "       " << abs(tat[i]) << "           |";
		}
		else {
			std::cout << "      " << abs(tat[i]) << "             |";
		}
	}

	cout << "\nAverage Waiting Time:  " << (double)twt / n << "\nAverage Turn Around Time: " << (double)ttat / n << endl;
	cout << "Scheduling complete\n";
}

int sched::getNextProcess(int time) {

	int i, low;
	for (i = 0;i<n;i++)
		if (finish[i] == 0) { low = i; break; }
	for (i = 0;i<n;i++)
		if (finish[i] != 1)
			if (rt[i]<rt[low] && at[i] <= time)
				low = i;
	return low;

}