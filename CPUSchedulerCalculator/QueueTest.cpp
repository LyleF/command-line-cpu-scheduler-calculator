#include "stdafx.h"
#include "QueueTest.h"

void QueueTest::inputTest() 
{
	int inputFile;

	std::cout << "Input 3 numbers" << "\n";
	for (int i = 0; i < 3; i++)
	{
		int currentNumber = i;
		std::cout << "Number " << ++currentNumber << "Size of Queue :" << QueueTest::testQueue.size() << "\n";
		std::cin >> inputFile;

		QueueTest::testQueue.push(inputFile);
	}
}

void QueueTest::outputContent()
{
	std::cout << "Size of Queue:" << QueueTest::testQueue.size() << "\n"
		<< "Content of Queue" << "\n";

	for (int i = 0; i < 3; i++)
	{
		int front = QueueTest::testQueue.front();
		std::cout << "Content: " << front << "\n";
		QueueTest::testQueue.pop();
	}
}