#include "stdafx.h"


void StateMachine::setCurrent(State *state)
{
	State *previousState = currentState;
	if (previousState != nullptr)
	{
		previousState->onExit(this);
		delete previousState;
	}

	currentState = state;
	currentState->onEnter(this);
}
