#pragma once
#pragma region State Machine Base Class 

class StateMachine
{
	class State *currentState;

public:
	StateMachine() {};
	virtual ~StateMachine() {};

	void setCurrent(State *state);

};

class State
{
public:
	virtual void onEnter(StateMachine *fsm) {};
	virtual void onExit(StateMachine *fsm) {};
};


#pragma endregion