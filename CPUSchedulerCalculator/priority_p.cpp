#pragma once
#include "stdafx.h"

void Priority_P::init(SchedulerData dataArray[], int size) 
{
	int i = 0;
	numberOfProcesses = size;
	arrivalTime = (int*)malloc(sizeof(int)*numberOfProcesses);
	CPUBurstTime = (int*)malloc(sizeof(int)*numberOfProcesses);
	CPUBurstTimeCopy = (int*)malloc(sizeof(int)*numberOfProcesses);
	processNumber = (int*)malloc(sizeof(int)*numberOfProcesses);
	waitingTime = (int*)malloc(sizeof(int)*numberOfProcesses);
	priority = (int*)malloc(sizeof(int)*numberOfProcesses);

	for (i = 0; i < numberOfProcesses;i++) 
	{
		waitingTime[i] = 0;
		processNumber[i] = i;
		arrivalTime[i] = dataArray[i].arrivalTime;
		CPUBurstTime[i] = dataArray[i].burstTime;
		priority[i] = dataArray[i].priorityLevel;

		CPUBurstTimeCopy[i] = CPUBurstTime[i];
		totalCPUBurstTime += CPUBurstTime[i];
		if (minimumArrivalTime>arrivalTime[i])
			minimumArrivalTime = arrivalTime[i];
	}
	processSequenceForEachSecond = (int*)malloc(sizeof(int)*totalCPUBurstTime);

	calculateProcessSequence();
	printf("\nAverage Waiting Time     = %f\n", averageWaitingTime);
	printf("\nAverage Turn Around Time = %f\n\n", averageTurnAroundTime);

	drawGanttChart();

	getchar();getchar();
}

void Priority_P::calculateProcessSequence()
{
	int i, j, pNumber, prevProcess, tempCPUBurstTime, counter, prevProcesss;

;	for (i = minimumArrivalTime;i<totalCPUBurstTime + minimumArrivalTime;i++)
	{
		pNumber = findAptProcessNumber(i);
		processSequenceForEachSecond[i - minimumArrivalTime] = pNumber;
		CPUBurstTime[pNumber]--;
		/*
		claculating the waiting time of each process;
		*/
		for (j = 0;j<numberOfProcesses;j++)
			if (CPUBurstTime[j] != 0 && arrivalTime[j] <= i && j != pNumber)
				waitingTime[j]++;
	
	}

	/* claculating the size of gantt chart arrays*/
	ganttSize = 1;
	prevProcess = processSequenceForEachSecond[0];
	for (i = 0;i<totalCPUBurstTime;i++)
	{
		if (prevProcess != processSequenceForEachSecond[i])
			ganttSize++;
		prevProcess = processSequenceForEachSecond[i];
	}

	/* allocating the size of gantt chart arrays */
	processNumberGantt = (int*)malloc(sizeof(int)*ganttSize);
	CPUBurstTimeGantt = (int*)malloc(sizeof(int)*ganttSize);

	/* assigning the values to Gantt Chart Arrays */
	prevProcess = processSequenceForEachSecond[0];
	tempCPUBurstTime = 0;
	counter = 0;
	for (i = 0;i<totalCPUBurstTime;i++)
	{
		if (prevProcess != processSequenceForEachSecond[i])
		{
			processNumberGantt[counter] = prevProcess;
			CPUBurstTimeGantt[counter] = tempCPUBurstTime;
			counter++;
			tempCPUBurstTime = 0;
		}
		tempCPUBurstTime++;
		prevProcess = processSequenceForEachSecond[i];
	}

	CPUBurstTimeGantt[ganttSize - 1] = tempCPUBurstTime;
	processNumberGantt[ganttSize - 1] = prevProcess;


	/* calculating the averageWaitingTime and averageTurnAroundTime */
	averageWaitingTime = 0;
	averageTurnAroundTime = 0;
	out("");

	std::cout << " Process Table ";
	addHorizontalLine(5);
	std::cout << "\n| Process | Waiting Time |  Turn Around Time  |";
	int t = 65;
	for (i = 0;i<numberOfProcesses;i++)
	{
		averageWaitingTime += waitingTime[i];
	/*	printf("P%c", (char)(65 + processNumberGantt[i]));
		std::cout << "Wait time:" << waitingTime[i] << "Burst:"<<CPUBurstTimeCopy[i] + waitingTime[i];*/
		int turnAroundTime = CPUBurstTimeCopy[i] + waitingTime[i];

		std::cout << "\n|    " << (char)(t++) << "    |";
		if (waitingTime[i]  > 9)
		{
			std::cout << "     " << abs(waitingTime[i]) << "       |";
		}
		else {
			std::cout << "     " << abs(waitingTime[i]) << "        |";
		}

		if (turnAroundTime > 9)
		{
			std::cout << "       " << abs(turnAroundTime) << "           |";
		}
		else {
			std::cout << "      " << abs(turnAroundTime) << "             |";
		}

		averageTurnAroundTime += turnAroundTime;
	}
	averageWaitingTime = averageWaitingTime / (float)numberOfProcesses;
	averageTurnAroundTime = averageTurnAroundTime / (float)numberOfProcesses;

}

int Priority_P::findAptProcessNumber(int currentTime)
{
	int i, min = 2147483647, pNumber = 0;
	for (i = 0;i<numberOfProcesses;i++)
		if (arrivalTime[i] <= currentTime && min>priority[i] && CPUBurstTime[i] != 0)
		{
			min = priority[i];
			pNumber = i;
		}
	return pNumber;
}

void Priority_P:: drawGanttChart()
{
	const int maxWidth = 100;
	int scalingFactor, i, counter, tempi, currentTime;
	printf("The gantt chart for the given processes is : \n\n");

	scalingFactor = maxWidth / totalCPUBurstTime;
	for (i = 0;i<scalingFactor*totalCPUBurstTime + 2 + ganttSize;i++)
		printf("-");
	printf("\n|");
	counter = 0, tempi = 0;
	for (i = 0;i<scalingFactor*totalCPUBurstTime;i++)
		if (i == CPUBurstTimeGantt[counter] * scalingFactor + tempi)
		{
			counter++;
			tempi = i;
			printf("|");
		}
		else if (i == (CPUBurstTimeGantt[counter] * scalingFactor) / 2 + tempi)
			printf(" %c", (char)(65 + processNumberGantt[counter]));
		else
			printf(" ");
	printf("|");
	printf("\n");
	for (i = 0;i<scalingFactor*totalCPUBurstTime + 2 + ganttSize;i++)
		printf("-");
	printf("\n");

	/* printing the time labels of the gantt chart */
	counter = 0;
	tempi = 0;
	currentTime = minimumArrivalTime;
	printf("%d", currentTime);
	for (i = 0;i<scalingFactor*totalCPUBurstTime;i++)
		if (i == CPUBurstTimeGantt[counter] * scalingFactor + tempi)
		{
			tempi = i;
			currentTime += CPUBurstTimeGantt[counter];
			averageWaitingTime += currentTime;
			counter++;
			printf("%2d", currentTime);
		}
		else
		{
			printf(" ");
		}
	currentTime += CPUBurstTimeGantt[counter];
	printf("%2d\n\n", currentTime);
}