// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include "istream"
#include "string"
#include "algorithm"
#include "list"

#include <stdio.h>
#include <tchar.h>
#include <iostream>

#include "StateMachine.h"
#include "CPUSchedAlgorithm.h"
#include "Program.h"
#include "sched.h"

#define out(x) std::cout<<x <<std::endl
#define in (x) std::cin>>x



// TODO: reference additional headers your program requires here
