#include "stdafx.h"
#include "InputFSM.h"
#include "InputStates.h"
void InputFSM::setCharacter(InputFSM *fsm, char c)
{
	switch (c) 
	{
		case 'a':
			fsm->setCurrent(new Left());
			break;
		case 'A':
			fsm->setCurrent(new Left());
			break;

		case 'D':
			fsm->setCurrent(new Right());
			break;
		case 'd':
			fsm->setCurrent(new Right());
			break;
	}
}