#pragma once
#include "stdafx.h"
#include<iostream>
#include<iomanip>
using namespace std;


void Priority_NP::getdata(SchedulerData dataArray[], int size)
{
	//cout << "How many process to be entered : ";
	//cin >> n;
	n = size;
	for (int i = 0;i<size;i++)
	{
		//cout << "Enter execution,arrival time and priority of " << i + 1 << " process : ";
		/*cin >> exe[i] >> ar[i] >> pr[i]; */
		id[i] = i + 1;
		exe[i] = dataArray[i].burstTime;
		ar[i] = dataArray[i].arrivalTime;
		pr[i] = dataArray[i].priorityLevel;
	}
}

void Priority_NP::sort(int *f, int *mid, int *last, int *ll)
{
	int temp;
	for (int y = 0;y<n - 1;y++)
	{
		for (int z = 0;z<n - 1;z++)
			if (f[z]>f[z + 1])
			{
				temp = f[z];
				f[z] = f[z + 1];
				f[z + 1] = temp;
				temp = mid[z];
				mid[z] = mid[z + 1];
				mid[z + 1] = temp;
				temp = last[z];
				last[z] = last[z + 1];
				last[z + 1] = temp;
				temp = ll[z];
				ll[z] = ll[z + 1];
				ll[z + 1] = temp;
			}
	}
}
void Priority_NP::cal_wt_tt()
{
	int flag = 1;
	int at = 0, ind, wt, tnt, min, max = pr[0];
	float avg = 0, avtnt = 0;
	sort(ar, id, exe, pr);
	at = ar[0];
	sort(pr, id, exe, ar);
	for (int i = 1;i<n;i++)
		if (max<pr[i])
			max = pr[i];
	min = max + 1;
	list<SchedulerData>* dataList = new list<SchedulerData>();
	list<SchedulerData> *tabulatedList = new list<SchedulerData>();
	while (flag)
	{
		for (int i = 0;i<n;i++)
		{
			if (at >= ar[i] && min>pr[i] && id[i]>0)
			{
				ind = i;
				min = pr[i];
			}
		}
		if (id[ind] == -1)
		{
			at++;
			continue;
		}
		wt = at - ar[ind];
		int res = at;
		at += exe[ind];
		int fin = at;
		avg += wt;
		tnt = at - ar[ind];
		avtnt += tnt;

		SchedulerData data;
		data.responseTime = res;
		data.timeFinished = fin;
		data.processChar = (char)(64 + id[ind]);
		dataList->emplace_back(data);

		SchedulerData tab;
		tab.waitingTime = wt;
		tab.turnAroundTime = tnt;
		tab.processChar = (char)(64 + id[ind]);
		tabulatedList->emplace_back(tab);

		id[ind] = -1;
		min = max + 1;
		flag = 0;
		for (int k = 0;k<n;k++)
			if (id[k] != -1)
				flag = 1;
	}
	out("");
	for (list<SchedulerData>::iterator it = dataList->begin(); it != dataList->end();++it)
	{
		cout << "|   " << it->processChar <<"   ";
	}
	out("");
	for (list<SchedulerData>::iterator it = dataList->begin(); it != dataList->end();++it)
	{
		//cout << "|[" << it->responseTime << "]" << it->processChar << "[" << it->timeFinished << "]|";
		if (it == dataList->begin() || it == dataList->end())
			cout << it->responseTime << "       " << it->timeFinished;
		else
			cout << "       " << it->timeFinished;
	}

	out("");
	out("");
	tabulatedList->sort([](const SchedulerData &x, const SchedulerData &y) {return x.processChar < y.processChar;});
	cout << "\nProcess ID \tWaiting time \tTurn Around time " << endl;
	for (list<SchedulerData>::iterator it = tabulatedList->begin(); it != tabulatedList->end();++it)
	{
		cout << setw(5) << it->processChar << setw(15) << it->waitingTime << setw(15) << it->waitingTime << endl;
	}
	delete dataList;
	avg = avg / (float)n;
	avtnt /= (float)n;
	cout << "\nAverage Waiting time     : " << avg;
	cout << "\nAverage turn Around time : " << avtnt << endl;
}