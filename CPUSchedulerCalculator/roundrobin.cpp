#pragma once
#include "stdafx.h"
#include "iostream"
using namespace std;
void RoundRobin::init(SchedulerData dataArray[], int size) {
	this->size = size;
	for (int x = 0;x<size;x++) 
	{
		at[x] = dataArray[x].arrivalTime;
		bt[x] = dataArray[x].burstTime;
		btm[x] = bt[x];
	}

	cout << "\nEnter time quantum:";
	cin >> qt;
}


void RoundRobin::disp()
{
	cout << endl << "GANTT CHART" <<endl;
	std::list<int> timeCollection;
	do {
		if (flg == 0) {
			st = at[0];
			//---ReduceBT
			if (btm[0] <= qt) {
				tm = st + btm[0];
				btm[0] = 0;
				search01(pnt, tm);
			}
			else {
				btm[0] = btm[0] - qt;
				tm = st + qt;
				search01(pnt, tm);
				addqueue(pnt);
			}
		}//if

		else {
			pnt = rqi[0] - 1;
			st = tm;
			//---DeleteQue
			for (int x = 0;x<noe && noe != 1;x++) {
				rqi[x] = rqi[x + 1];
			}
			noe--;
			//---ReduceBT
			if (btm[pnt] <= qt) {
				tm = st + btm[pnt];
				btm[pnt] = 0;
				search02(pnt, tm);
			}
			else {
				btm[pnt] = btm[pnt] - qt;
				tm = st + qt;
				search02(pnt, tm);
				addqueue(pnt);
			}
		}//else

		 //AssignCTvalue
		if (btm[pnt] == 0) {
			ct[pnt] = tm;
		}//if

		flg++;
		cout << "|  " << (char)(65 + pnt) /*+ 1 */ << "   ";
		timeCollection.emplace_back(tm);

	} while (noe != 0);

	out("");
	cout << at[0];
	for (list<int>::iterator it = timeCollection.begin(); it != timeCollection.end(); it++) 
	{
		cout << "      " << *it;
	}
	out("");
	out("");
	cout << "\n\nPROCESS\t Waiting Time\t Turn Around Time\n";
	for (int x = 0;x<size;x++) {
		tt = ct[x] - at[x];
		wt = tt - bt[x];
		cout << (char)(65 + x) << " \t " << wt << " \t \t \t " << tt << "\n";
		awt = awt + wt;
		att = att + tt;
	}//for

	cout << "\nAVERAGE Turn Around Time: " << att / 5 << "\nAVERAGE Wait Time: " << awt / 5;
	getchar();
}

void RoundRobin::search01(int pnt, int tm) {
	for (int x = pnt + 1;x<size;x++) {
		if (at[x] <= tm) {
			rqi[noe] = x + 1;
			noe++;
		}
	}

}
void RoundRobin::search02(int pnt, int tm) {
	for (int x = pnt + 1;x<size;x++) {
		//---CheckQue
		int fl = 0;
		for (int y = 0;y<noe;y++) {
			if (rqi[y] == x + 1) {
				fl++;
			}
		}
		if (at[x] <= tm && fl == 0 && btm[x] != 0) {
			rqi[noe] = x + 1;
			noe++;
		}
	}

}
void RoundRobin::addqueue(int pnt) {
	rqi[noe] = pnt + 1;
	noe++;
}