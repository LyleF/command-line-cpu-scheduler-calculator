#pragma once
#include "stdafx.h"


void EnqueueState::onEnter(StateMachine *machine)
{
	SchedulerAlgorithm *scheduler = dynamic_cast<SchedulerAlgorithm*>(machine);
	for (std::list<SchedulerData>::iterator it = scheduler->currentAvailableDataList->begin(); it != scheduler->currentAvailableDataList->end(); ++it)
	{
		SchedulerData &data = *it;

		//if (dynamic_cast<FirstComeFirstServe*>(machine) || dynamic_cast<ShortestJobFirst_NP*>(machine))
		//{
		//	if (it == scheduler->currentAvailableDataList->begin())
		//	{
		//		data.responseTime = data.arrivalTime;
		//	}
		//	else
		//	{
		//		data.responseTime = prevData.timeFinished;
		//	}

	//		data.timeFinished = data.burstTime + data.responseTime;
			scheduler->currentProcessQueue->push(data);
	/*		prevData = data;
		}*/
	}

	scheduler->programMachine->setCurrent(new Display(scheduler->currentDataList, scheduler->currentAvailableDataList, scheduler->currentProcessQueue, false));
}

void EnqueueState::onExit(StateMachine *machine) 
{
	delete machine;
}