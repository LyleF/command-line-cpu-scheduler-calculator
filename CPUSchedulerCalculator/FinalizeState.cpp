#include "stdafx.h"

void FinalizeState::onEnter(StateMachine *machine) 
{
	SchedulerAlgorithm *scheduler = dynamic_cast<SchedulerAlgorithm*>(machine);
	SchedulerData prevData;
	for (std::list<SchedulerData>::iterator it = scheduler->currentAvailableDataList->begin(); it != scheduler->currentAvailableDataList->end(); ++it)
	{
		SchedulerData &data =*it;
		data.waitingTime = data.responseTime - data.arrivalTime;
		data.turnAroundTime = data.timeFinished - data.arrivalTime;
	}

	scheduler->programMachine->setCurrent(new Display(scheduler->currentDataList,scheduler->currentAvailableDataList,scheduler->currentProcessQueue, false));
}

void FinalizeState::onExit(StateMachine *machine) 
{
	delete machine;
}