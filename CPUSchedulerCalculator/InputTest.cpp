#include "stdafx.h"
#include "InputTest.h"
#include "InputFSM.h"

#include <iostream>
#include <conio.h>

char activeCharacter;

void doInput(StateMachine *machine) 
{
	bool doPrompt = true;

	while (true)
	{
		if (doPrompt)
		{
			std::cout << "Input a letter: ";
			doPrompt = false;
		}

		while (_kbhit())
		{
			doPrompt = true;
			activeCharacter = _getch();
			std::cout << activeCharacter;

			if (activeCharacter == 27)
			{
				goto here;
			}
			
			InputFSM* test = dynamic_cast<InputFSM*> (machine);
			std::cout << "\n";
			test->setCharacter(test, activeCharacter);
		}
	}

	here:

	std::cout << "\n";
}

char getInputCharacter() 
{
	return activeCharacter;
}