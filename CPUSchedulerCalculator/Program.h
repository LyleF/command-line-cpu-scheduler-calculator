#pragma once
#include "StateMachine.h"
#include "CPUSchedAlgorithm.h"

#pragma region  States
class Input : public State 
{
public:
	void onEnter(StateMachine * machine) override;
	void onExit(StateMachine * machine) override;
private:
	void getInputData(StateMachine *machine);
	void setSchedulerData(std::list<SchedulerData> *data, int amount);
	void displayMenu(StateMachine *machine);
	void displaySchedulerChoices(StateMachine *machine);
	void waitForInput(StateMachine *machine, bool fromMenu);
	std::list<SchedulerData> *currentDataList;
	int validateNumberInput(char msg[]);
	char choice;
};
class Display : public State 
{
	public:
		Display(std::list<SchedulerData> *dataList, std::list<SchedulerData> *currentAvailableDataList, std::queue<SchedulerData> *queuedData, bool forceDisp);
		void onEnter(StateMachine * machine) override;
		void onExit(StateMachine * machine) override;
	private:
		void displayProcessTable(bool isFinal);
		std::list<SchedulerData> *currentDataList;
		std::list<SchedulerData> *currentAvailableData;
		std::queue<SchedulerData> *currentQueuedData;
		bool forceDisp;
};
#pragma endregion
class Program : public StateMachine
{
	public:
		Program();	
	
};